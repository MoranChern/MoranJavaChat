package mjc.mjcm;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import mjc.mjcp.*;

public class ClientUI extends JFrame {
    private ClientLessonView lessonView;
    private ClientDrawView drawView;

    private ObjectInputStream in;
    private ObjectOutputStream out;
    private String clientName;
    private Socket socket;
    private boolean online=true;
    
    private JLabel notifier = null;

    private JList<String> userList;
    private DefaultListModel<String> userListModel;
    private ArrayList<ChatView> userChatViews=new ArrayList<>();

    private JList<String> groupList;
    private DefaultListModel<String> groupListModel;
    private ArrayList<GroupChatView> groupChatViews=new ArrayList<>();
    

    public ClientUI(String name,List<String> clientNames,List<GroupInfo> groups,ObjectInputStream in,ObjectOutputStream oos, Socket socket) {
        super(name+" — 聊天列表");
        
        this.clientName = name;
        this.in = in; 
        this.out = oos;
        this.socket=socket;

        lessonView = new ClientLessonView(clientName,this.out);
        drawView = new ClientDrawView(this.clientName);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
        JPanel topPanel = new JPanel(new BorderLayout());

        notifier = new JLabel("  ");
        topPanel.add(notifier, BorderLayout.NORTH);

        JButton groupCreate = new JButton("创建群");
        groupCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SubmitView(new GroupCreateCallback(),"创建群聊","请输入群聊名称",true);
            }
        });
        topPanel.add(groupCreate, BorderLayout.EAST);

        JButton drawPanel = new JButton("课堂画板");
        drawPanel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drawView.setVisible(true);
            }
        });
        topPanel.add(drawPanel, BorderLayout.CENTER);

        JButton lessonChat = new JButton("课堂互动");
        lessonChat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lessonView.setVisible(true);
            }
        });
        topPanel.add(lessonChat, BorderLayout.WEST);

        getContentPane().add(topPanel, BorderLayout.NORTH);


        ///////////////////////////////////////////////////////////
        JPanel listPanel = new JPanel();

        // 初始化列表模型
        userListModel = new DefaultListModel<>();
        for(String i:clientNames) {
            addUser(i);
        }
        // 创建列表
        userList = new JList<>(userListModel);
        // 添加鼠标双击事件监听器
        userList.addMouseListener(new UserChatTrigger());
        // 将列表添加到滚动面板中
        JScrollPane userScrollPane = new JScrollPane(userList);
        listPanel.add(userScrollPane);
        
        // 初始化列表模型
        groupListModel = new DefaultListModel<>();
        for(GroupInfo i:groups) {
            addGroup(i);
        }
        // 创建列表
        groupList = new JList<>(groupListModel);
        // 添加鼠标双击事件监听器
        groupList.addMouseListener(new GroupChatTrigger());
        // 将列表添加到滚动面板中
        JScrollPane groupScrollPane = new JScrollPane(groupList);
        listPanel.setLayout(new BoxLayout(listPanel, BoxLayout.Y_AXIS));
        listPanel.add(new JLabel("用户列表"));
        listPanel.add(userScrollPane);
        listPanel.add(new JLabel("群组列表"));
        listPanel.add(groupScrollPane);

        
        getContentPane().add(listPanel, BorderLayout.CENTER);
        ///////////////////////////////////////////////////////////



        // 设置窗体大小并显示
        setSize(300, 300);
        setVisible(true);
        new Receiver().start();
        new Notify("您已上线").start();
    }
    
    private class GroupCreateCallback extends SubmitCallback{
        @Override
        public String call() throws Exception {
            if(view.getStatus()){
                String name=((SubmitView)view).getValue();
                System.out.println("正在创建群聊 "+ name);
                out.writeObject(new GroupCreate(name));
            } else{System.err.println("错误的回调函数调用！");}
            return null;
        }
    }

    private class BeInvitedCallback extends SubmitCallback{
        private String groupName;
        BeInvitedCallback(String groupName){this.groupName = groupName;}

        @Override
        public String call() throws Exception {
            if(view.getStatus()){
                boolean accepted=((ConfirmView)view).getValue();
                if(accepted) {
                    out.writeObject(new GroupAccept(this.groupName));
                }else{
                    out.writeObject(new GroupDecline(this.groupName));
                }
            } else{System.err.println("错误的回调函数调用！");}
            return null;
        }        
    }

    class UserChatTrigger extends MouseAdapter {
        private ChatView chatView=null;

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                int index = userList.locationToIndex(e.getPoint());
                String item = userListModel.getElementAt(index);

                //第一次启动的匹配
                if(this.chatView==null||(!item.equals(this.chatView.getClientName()))){
                    for(ChatView i : userChatViews){
                        if(i.getClientName().equals(item)){
                            this.chatView=i;
                            break;
                        }
                    }
                }
                this.chatView.setVisible(true);
            }
        }
    }   

    class GroupChatTrigger extends MouseAdapter {
        private GroupChatView groupChatView=null;

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                int index = groupList.locationToIndex(e.getPoint());
                String item = groupListModel.getElementAt(index);

                // 第一次启动的匹配
                if(this.groupChatView==null||(!item.equals(this.groupChatView.getGroupName()))){
                    for(GroupChatView i : groupChatViews){
                        if(i.getGroupName().equals(item)){
                            this.groupChatView=i;
                            break;
                        }
                    }
                } else if(!this.groupChatView.getStatus()){
                    this.groupChatView.reborn();
                }
                      
                this.groupChatView.reborn();
                this.groupChatView.setVisible(true);
            }
        }
    }  

    public boolean addUser(String user) {
        if (!userListModel.contains(user)&&!user.equals(clientName)){ 
            userListModel.addElement(user);
            userChatViews.add(new ChatView(clientName, user,out));
            return true;
        }
        return false;
    }

    public boolean addGroup(GroupInfo group) {
        String groupName = group.getGroupName();
        if (!groupListModel.contains(groupName)){ 
            groupListModel.addElement(groupName);
            groupChatViews.add(new GroupChatView(clientName, group,out));
            return true;
        }
        return false;
    }

    public boolean removeUser(String user) {
        if (userListModel.contains(user)) {
            userListModel.removeElement(user);
            for(ChatView i : userChatViews){
                if(i.getClientName().equals(user)){
                    userChatViews.remove(i);
                    break;
                }
            }
            return true;
        }
        return false;
    }

    public boolean removeGroup(String group) {
        if (groupListModel.contains(group)) {
            groupListModel.removeElement(group);
            for(GroupChatView i : groupChatViews){
                if(i.getGroupName().equals(group)){
                    groupChatViews.remove(i);
                    break;
                }
            }
            return true;
        }
        return false;
    }

    private class Receiver extends Thread{
        @Override
        public void run(){
            try{
            while(online){
            Object obj = in.readObject();
            if (obj instanceof MJCPPackage){
                System.out.println("收到报文");                
                
                /* not reachable*/if(!(obj instanceof MJCPPackage)){;}

                // ServerDeny////////////////////////////////////
                else if(obj instanceof ServerDeny) {
                    System.out.println("收到ServerDeny");
                    MJCPPackage error=((ServerDeny)obj).getError();
                    if(!(error instanceof MJCPPackage)){}
                    else if(error instanceof GroupCreate) {
                        GroupCreate groupcreate=(GroupCreate)error;
                        new Notify("群组 "+groupcreate.getGroupName()+" 创建失败").start();
                        System.out.println("群组 "+groupcreate.getGroupName()+" 创建失败");
                    }
                    else if(error instanceof GroupInvite){
                        String name=((GroupInvite)error).getGroupName();
                        for(GroupChatView i:groupChatViews){
                            if(i.getGroupName().equals(name)){
                                i.push((ServerDeny)obj);
                                break;
                            }
                        }
                    }
                }
                // ServerMessage ////////////////////////////////
                else if (obj instanceof ServerMessage){
                    ServerMessage servermessage=(ServerMessage)obj;
                    new Notify("服务器广播："+servermessage.getMsg()).start();
                    System.out.println("服务器广播："+servermessage.getMsg());
                }
                // ClientHello///////////////////////////////////
                // GroupDeny/////////////////////////////////////
                // GroupInvite///////////////////////////////////
                else if(obj instanceof GroupInvite){
                    GroupInvite ivitation=(GroupInvite)obj;
                    String groupName=ivitation.getGroupName();
                    String host=ivitation.getHostName();
                    new ConfirmView(new BeInvitedCallback(groupName),"群聊邀请",host+" 邀请您加入群聊 "+groupName+" 是否同意？",false);
                }
                // Message///////////////////////////////////////
                else if(obj instanceof Message){
                    System.out.println("收到用户消息");
                    Message message=(Message)obj;
                    for(ChatView i: userChatViews){
                        if(i.getClientName().equals(message.getSrc())){
                            i.push(message);                            
                            System.out.println("推送消息到 "+i.getClientName());
                            break;
                        }
                    }
                }
                // GroupAccept///////////////////////////////////
                // GroupDismiss//////////////////////////////////
                // GroupKick/////////////////////////////////////
                // MJCPPackag////////////////////////////////////
                // UserUp////////////////////////////////////////
                else if(obj instanceof UserUp){
                    UserUp userup=(UserUp)obj;
                    addUser(userup.getClientName());
                    Notify n=new Notify(userup.getClientName()+" 上线了");
                    n.start();
                    System.out.println(userup.getClientName()+" 上线了");
                }
                // GroupUp///////////////////////////////////////
                else if(obj instanceof GroupUp){
                    GroupUp groupUp=(GroupUp)obj;
                    addGroup(groupUp.getGroupInfo());
                    Notify n=new Notify("您已加入群组 "+groupUp.getGroupName());
                    n.start();
                    System.out.println("您已加入群组 "+groupUp.getGroupName());
                }
                // UserDown//////////////////////////////////////
                else if(obj instanceof UserDown){
                    UserDown userdown=(UserDown)obj;
                    for(ChatView i: userChatViews){
                        if(i.getClientName().equals(userdown.getClientName())){
                            i.push(userdown);
                            break;
                        }
                    }
                    removeUser(userdown.getClientName());
                    new Notify(userdown.getClientName()+" 下线了").start();
                    System.out.println(userdown.getClientName()+" 下线了");
                }
                // GroupDown/////////////////////////////////////
                else if(obj instanceof GroupDown){
                    GroupDown groupdown=(GroupDown)obj;
                    for(GroupChatView i: groupChatViews){
                        if(i.getGroupName().equals(groupdown.getGroupName())){
                            i.push(groupdown);
                            // System.out.println("成功推送退群报文！");
                            break;
                        }
                    }
                    removeGroup(groupdown.getGroupName());
                    new Notify("群组 "+groupdown.getGroupName()+" 已经不再可用").start();
                    System.out.println("群组 "+groupdown.getGroupName()+" 已经不再可用");
                }
                // ServerHello///////////////////////////////////
                else if (obj instanceof ServerHello) {
                    ServerHello serverhello=(ServerHello)obj;
                    System.out.println("收到列表更新");
                    System.out.println(serverhello.getUserList());
                    // List<String> newusers=serverhello.getUserList();
                    // List<GroupInfo> newgroups=serverhello.getGroupList();
                    // for(String i:newusers){
                    //     addUser(i);
                    // }
                    // for(ChatView i:chatViews){
                    //     if(!newusers.contains(i.getClientName()) ){
                    //         removeUser(i.getClientName());
                    //     }
                    // }
                }
                // GroupCreate///////////////////////////////////
                // GroupListUpdate///////////////////////////////
                // GroupMessage//////////////////////////////////
                else if(obj instanceof GroupMessage){
                    System.out.println("收到群消息");
                    GroupMessage groupMessage=(GroupMessage)obj;
                    for(GroupChatView i: groupChatViews){
                        if(i.getGroupName().equals(groupMessage.getGroupName())){
                            i.push(groupMessage);                            
                            System.out.println("推送消息到 "+i.getGroupName());
                            break;
                        }
                    }                    
                }
                // GroupDecline//////////////////////////////////
                // GroupExit/////////////////////////////////////
                // GroupMemberUp//in GroupListUpdate/////////////
                else if(obj instanceof GroupMemberUp){
                    GroupMemberUp groupMemberUp = (GroupMemberUp)obj;
                    for(GroupChatView i:groupChatViews){
                        if(i.getGroupName().equals(groupMemberUp.getGroupName())){
                            i.push(groupMemberUp);
                            break;
                        }
                    }
                }
                // GroupMemberDown//in GroupListUpdate///////////
                else if(obj instanceof GroupMemberDown){
                    GroupMemberDown groupMemberDown = (GroupMemberDown)obj;
                    for(GroupChatView i:groupChatViews){
                        if(i.getGroupName().equals(groupMemberDown.getGroupName())){
                            i.push(groupMemberDown);
                            break;
                        }
                    }
                }
                // LessonMessage ///////////////////////////////
                else if(obj instanceof LessonMessage){
                    LessonMessage lessonMessage=(LessonMessage)obj;
                    lessonView.push(lessonMessage);
                }
                // LessonDraw //////////////////////////////////
                else if(obj instanceof DrawPoint||obj instanceof DrawPenUp||obj instanceof DrawClear||obj instanceof DrawChangePen || obj instanceof DrawRect){
                    drawView.push((MJCPPackage)obj);
                }
                else if(obj instanceof FileSend){
                    ClientFileReceiver receiver = new ClientFileReceiver(clientName, (FileSend)obj);
                    out.writeObject(new FileReceive(clientName, InetAddress.getLocalHost().getHostAddress(), receiver.getPort()));
                    new Notify("正在接受新文件，客户端将暂停运行！").start();
                    if(receiver.receive()){
                        new Notify("接收到新文件！").start();
                    }
                }
            }
            }
            }catch(EOFException ex){
                //断开连接
                System.out.println("服务器已断开！");
                stopClient();
            }catch (IOException | ClassNotFoundException ex) {
                System.out.println("服务端捕获到异常！");
                ex.printStackTrace();
            }

        }
    }

    private class Notify extends Thread{
        private static Notify newest=null; 
        public Notify(String message) {
            notifier.setText(message);
            // this.start();
        }
        @Override
        public void run() {
            try {
                newest=this;
                sleep(3000);
                if(newest==this){
                    notifier.setText("");
                }
                // else{System.out.println("两个通知！");}
            } catch (InterruptedException e) {System.err.println("显示消息延时时出现异常！");}
        }
    }

    public void stopClient(){
        System.out.println("客户端UI停止运行！");
        online=false;        
        try {
            in.close();
            out.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}