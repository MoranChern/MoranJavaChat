package mjc.mjcm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class ConfirmView extends MJCModule {
    private boolean disposablity=false;
    private SubmitCallback caller;
    private boolean value = false;
    private JLabel jl1 = null;
    private JButton jb1 = new JButton("是");
    private JButton jb2 = new JButton("否");
    
    public boolean getValue(){return this.value;}

    public ConfirmView(SubmitCallback callable,String title, String note,boolean allowDispose){
        this.disposablity = allowDispose;
        this.caller = callable;
        caller.setSubmit(this);

        this.setTitle(title);
        this.setSize(400, 150);
        if(this.disposablity){
            this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        }else{
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        }
        this.setVisible(true);
        this.setLayout(null);
        
        jl1=new JLabel(note);
        jl1.setBounds(0, 0, 400, 20);
        this.add(jl1);

        jb1.setBounds(250, 80, 100, 20);
        jb1.addActionListener(new trueListener());
        this.add(jb1);

        jb2.setBounds(50, 80, 100, 20);
        jb2.addActionListener(new falseListener());
        this.add(jb2);

    }
    private class trueListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(status){
                jl1.setText("重复调用，请注意！");
            }else{
                value=true;
                status=true;
                setVisible(false);
                try {
                    caller.call();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    System.err.println("在确认界面回调时出现异常！");
                }
            }
        }
    }    
    private class falseListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(status){
                jl1.setText("重复调用，请注意！");
            }else{
                value=false;
                status=true;
                setVisible(false);
                try {
                    caller.call();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    System.err.println("在确认界面回调时出现异常！");
                }
            }
        }
    }  


}