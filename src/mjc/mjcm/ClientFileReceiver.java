package mjc.mjcm;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

import mjc.Config;
import mjc.mjcp.FileSend;

public class ClientFileReceiver {
    private String fileName;
    private boolean done = false;
    private Socket socket = null;
    private int port =-1;

    public ClientFileReceiver(String ClientName,FileSend sender) {
        this.fileName = Config.FILE_RECEIVE_AREA + File.separator + ClientName + File.separator + sender.getFileName();
        File userDirectory = new File(Config.FILE_RECEIVE_AREA + File.separator + ClientName);
        if(!userDirectory.exists()){userDirectory.mkdirs();}
        Random random = new Random();
        port = random.nextInt(999)+7000;
    }

    public int getPort(){return this.port;}

    public boolean receive() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            new DeadCountdown().start();
            System.out.println("等待文件传入...");
            socket = serverSocket.accept();

            byte[] buffer = new byte[4096];
            InputStream is = socket.getInputStream();
            FileOutputStream fos = new FileOutputStream(fileName);

            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
            }
            done=true;

            fos.close();
            System.out.println("文件接收完成.");
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    private class DeadCountdown extends Thread{
        @Override
        public void run() {
            try {
                sleep(20000);
                if(!done){
                    socket.close();
                    System.err.println("接收文件超时！");
                }
            } catch (InterruptedException e) {
                System.err.println("接收器倒计时出错！");
            } catch (IOException e) {}
        }
    }
}