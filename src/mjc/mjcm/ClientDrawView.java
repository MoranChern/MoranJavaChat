package mjc.mjcm;

import javax.swing.*;

import mjc.mjcp.DrawPoint;
import mjc.mjcp.DrawRect;
import mjc.mjcp.DrawChangePen;
import mjc.mjcp.DrawClear;
import mjc.mjcp.DrawPenUp;
import mjc.mjcp.MJCPPackage;

import java.awt.*;

public class ClientDrawView extends JFrame {
    private String clientName;
    private JPanel drawingPanel;
    private int prevX, prevY;
    private boolean drawing=false;
    private Color color=Color.black;
    private int radius=2;

    public ClientDrawView(String name) {
        this.clientName = name;
        setTitle("教学白板（"+this.clientName+"）");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setPreferredSize(new Dimension(400, 700));
        setLayout(new BorderLayout());

        // 创建绘图区域
        drawingPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                // 在这里实现绘图逻辑
            }
        };
        drawingPanel.setPreferredSize(new Dimension(400, 500));
        add(drawingPanel, BorderLayout.CENTER);

        pack();
        setResizable(false);
        setVisible(false);
    }

    public void push(MJCPPackage pack){
        if(!(pack instanceof MJCPPackage)){;}
        
        else if(pack instanceof DrawPoint){
            DrawPoint lessonDraw = (DrawPoint)pack;
            if(this.drawing){
                int x=lessonDraw.getX();
                int y=lessonDraw.getY();
                Graphics2D g = (Graphics2D) drawingPanel.getGraphics();
                g.setColor(color);
                g.fillOval(x - radius, y - radius, radius*2, radius*2);

                // 绘制当前点和上一个点的连线
                g.setColor(color);
                g.setStroke(new BasicStroke(radius*2));
                g.drawLine(prevX, prevY, x, y);

                prevX = x;
                prevY = y;

            } else{
                drawing = true;
                prevX = lessonDraw.getX();
                prevY = lessonDraw.getY();
            }
        }

        else if(pack instanceof DrawRect){
            DrawRect drawRect = (DrawRect)pack;
            Graphics2D g = (Graphics2D) drawingPanel.getGraphics();
            g.drawRect(drawRect.getX(), drawRect.getY(), drawRect.getWidth(), drawRect.getHeight());
        }

        else if(pack instanceof DrawChangePen){
            DrawChangePen pen = (DrawChangePen)pack;
            this.radius = pen.getRadius();
            this.color = pen.getColor();
        }

        else if(pack instanceof DrawPenUp){
            this.drawing = false;
        }

        else if(pack instanceof DrawClear){
            drawingPanel.repaint();
        }
    }
}
