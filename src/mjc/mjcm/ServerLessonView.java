package mjc.mjcm;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import mjc.Server;
import mjc.mjcp.*;

public class ServerLessonView extends MJCModule{
    
    private Server caller;

    private JTextArea messageArea;
    private JTextField inputField;

    private boolean sendingFile = false;
    private String filePath = null;

    public void stop(){
        if(this.isVisible()){
            // setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);  
            this.status=false;
            messageArea.append("对话已下线，请关闭此窗口\n---------------------------\n");
        }
        // else{
        //     this.dispose();
        // }
    }

    public ServerLessonView(Server server) {
        this.caller = server;

        setTitle("课堂交流窗口（服务器）");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new BorderLayout());

        
        JPanel buttonPanel = new JPanel(new FlowLayout());         
        JButton sendFile = new JButton("发送文件");
        sendFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!sendingFile){
                    new SubmitView(new FileCallback(), "发送文件", "输入文件目录", true);
                }else{
                    messageArea.append("此时不允许发送文件，请稍候！");endl();
                }
            }
        });
        buttonPanel.add(sendFile);
        add(buttonPanel, BorderLayout.NORTH);

        // 创建消息输出区域
        messageArea = new JTextArea();
        messageArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(messageArea);
        add(scrollPane, BorderLayout.CENTER);

        // 创建输入框区域
        inputField = new JTextField();
        inputField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(status&&!inputField.getText().isEmpty()){                    
                    caller.broadcast(new LessonMessage("Server", inputField.getText()));
                    messageArea.append("Server :\n"+inputField.getText()+"\n---------------------------\n");
                    inputField.setText("");
                }
            }
        });
        add(inputField, BorderLayout.SOUTH);

        pack();
        this.setSize(400, 400);
        setVisible(false);
        this.status=true;
    }

    private class FileCallback extends SubmitCallback{
        @Override
        public String call() throws Exception {
            if(view.getStatus()){
                File file=new File(((SubmitView)view).getValue());
                if(file.exists()){
                    filePath = file.getAbsolutePath();
                    new SendCountdown().start();
                    caller.broadcast(new FileSend(file.getName()));
                    messageArea.append("正在发送文件“"+filePath+"”，20秒内请不要重复发送文件。");endl();
                }
                else{
                    messageArea.append("文件读取失败！");endl();
                }
            } else{System.err.println("错误的回调函数调用！");}
            return null;
        }
    }

    private class SendCountdown extends Thread{
        @Override
        public void run() {
            try {
                sendingFile=true;
                sleep(20000);
                sendingFile=false;
                filePath=null;
            } catch (InterruptedException e) {
                System.err.println("发送文件计时器发生错误");
            }
        }
    }    

    public void push(MJCPPackage pack){    
        if(!this.status)  {;}  
        else if (pack instanceof FileReceive){
            if(sendingFile){
                new ServerFileSender(filePath, (FileReceive)pack).start();
            }
        }
        // ClientHello///////////////////////////////////
        // GroupDeny/////////////////////////////////////
        // GroupInvite///////////////////////////////////
        // GroupMemberUpdate/////////////////////////////
        // Message///////////////////////////////////////
        else if(pack instanceof LessonMessage){
            LessonMessage lessonMessage=(LessonMessage)pack;
            messageArea.append(lessonMessage.getSrc()+" :\n"+lessonMessage.getContent());endl();
        }
        // UserDown//////////////////////////////////////
        // GroupAccept///////////////////////////////////
        // GroupDismiss//////////////////////////////////
        // GroupKick/////////////////////////////////////
        // GroupMemberUp//in GroupListUpdate/////////////
        // MJCPPackag////////////////////////////////////
        // UserUp////////////////////////////////////////
        // GroupCreate///////////////////////////////////
        // GroupDown/////////////////////////////////////
        // GroupListUpdate///////////////////////////////
        // GroupMessage//////////////////////////////////
        // ServerDeny////////////////////////////////////
        // GroupDecline//////////////////////////////////
        // GroupExit/////////////////////////////////////
        // GroupMemberDown//in GroupListUpdate///////////
        // GroupUp///////////////////////////////////////
        // ServerHello///////////////////////////////////
    }

    private void endl(){
        messageArea.append("\n---------------------------\n");
    }
}
