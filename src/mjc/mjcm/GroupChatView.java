package mjc.mjcm;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import mjc.mjcp.*;

public class GroupChatView extends MJCModule{
    
    private ObjectOutputStream out;
    private String clientName;
    private String host;
    private ArrayList<String> members=new ArrayList<>();
    // private GroupInfo groupInfo;
    private String groupName;

    private JTextArea messageArea;
    private JTextField inputField;

    public String getGroupName(){return this.groupName;}
    public void stopChat(){
        // if(this.isVisible()){
            // setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);  
            this.status=false;
            messageArea.append("对话已下线，请关闭此窗口\n---------------------------\n");
            // System.out.println("窗口已经准备关闭");
        // }
        // else{
        //     this.dispose();
        // }
    }
    boolean addMember(String memberName){
        boolean ret=!members.contains(memberName);
        if(ret){members.add(memberName);}
        return ret;
    }
    boolean removeMember(String memberName){
        boolean ret=members.contains(memberName);
        if(ret){members.remove(memberName);}
        return ret;
    }

    public GroupChatView(String self,GroupInfo group, ObjectOutputStream oos) {
        this.host = group.getHostName();
        this.groupName = group.getGroupName();
        this.members = group.getUserList();
        this.clientName = self;
        this.out = oos;

        setTitle(groupName+" — 群组聊天窗口（"+clientName+"）");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new BorderLayout());

        // 创建顶部按钮区域
        JPanel buttonPanel = new JPanel(new FlowLayout());        
        JButton usersGetter = new JButton("用户列表");
        usersGetter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                messageArea.append("群主：\n"+host+"\n群组成员：\n"+members);endl();
            }
        });
        buttonPanel.add(usersGetter);
        if(this.host.equals(this.clientName)){
            JButton inviteTrigger = new JButton("邀请");
            inviteTrigger.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new SubmitView(new InviteCallback(),"邀请新成员","请输入需要邀请用户的用户名",true);
                }
            });
            buttonPanel.add(inviteTrigger);
            JButton kickTrigger = new JButton("移除用户");
            kickTrigger.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new SubmitView(new KickCallback(),"移除成员","请输入需要移除用户的用户名",true);
                }
            });
            buttonPanel.add(kickTrigger);
            JButton dismissTrigger = new JButton("解散群聊");
            dismissTrigger.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new ConfirmView(new DismissCallback(),"解散确认","确认解散此群聊吗？",true);
                }
            });
            buttonPanel.add(dismissTrigger);
        }
        else{
            JButton exitTrigger = new JButton("退出群聊");
            exitTrigger.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    new ConfirmView(new ExitCallback(),"退出群聊确认","确认退出此群聊吗？",true);
                }
            });
            buttonPanel.add(exitTrigger);
        }
        add(buttonPanel, BorderLayout.NORTH);

        // 创建消息输出区域
        messageArea = new JTextArea();
        messageArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(messageArea);
        add(scrollPane, BorderLayout.CENTER);

        // 创建输入框区域
        inputField = new JTextField();
        inputField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(status&&!inputField.getText().isEmpty()){
                try {
                    out.writeObject(new GroupMessage(clientName, group.getGroupName(), inputField.getText()));
                    // System.out.println("to " + group.getGroupName()+" :\n"+inputField.getText());
                    // messageArea.append(clientName+" :\n"+inputField.getText()+"\n---------------------------\n");
                    inputField.setText("");
                } catch (IOException e1) {
                    System.err.println("发送信息到 "+group+" 时出现异常");
                    e1.printStackTrace();
                }}
            }
        });
        add(inputField, BorderLayout.SOUTH);

        pack();
        this.setSize(400, 400);
        setVisible(false);
        this.status=true;
    }

    private class DismissCallback extends SubmitCallback{
        @Override
        public String call() throws Exception {
            if(view.getStatus()){
                if(((ConfirmView)view).getValue()){                    
                    out.writeObject(new GroupDismiss(groupName));
                }
            } else{System.err.println("错误的回调函数调用！");}
            return null;
        }
    }

    private class ExitCallback extends SubmitCallback{
        @Override
        public String call() throws Exception {
            if(view.getStatus()){
                if(((ConfirmView)view).getValue()){                    
                    out.writeObject(new GroupExit(groupName));
                }
            } else{System.err.println("错误的回调函数调用！");}
            return null;
        }
    }

    private class InviteCallback extends SubmitCallback{
        @Override
        public String call() throws Exception {
            if(view.getStatus()){
                String name=((SubmitView)view).getValue();
                messageArea.append("已邀请 "+ name);endl();
                out.writeObject(new GroupInvite(clientName, name, groupName));
            } else{System.err.println("错误的回调函数调用！");} 
            return null;
        }
    }

    private class KickCallback extends SubmitCallback{
        @Override
        public String call() throws Exception {
            if(view.getStatus()){
                String name=((SubmitView)view).getValue();
                boolean nameExists=false;
                for(String s:members){
                    if(name.equals(s)){
                        nameExists=true;
                        break;
                    }
                }
                if(nameExists){
                    messageArea.append("已移除 "+ name);endl();
                    out.writeObject(new GroupKick(clientName, name, groupName));
                } else{
                    messageArea.append("无法移除！用户可能不在群组中。");endl();
                }
            } else{System.err.println("错误的回调函数调用！");}
            return null;
        }
    }


    public void push(MJCPPackage pack){    
        System.out.println("群聊窗口接受报文");
        if(!(pack instanceof MJCPPackage))  {;}  
        // ClientHello///////////////////////////////////
        // GroupDeny/////////////////////////////////////
        // GroupInvite///////////////////////////////////
        // Message///////////////////////////////////////
        // UserDown//////////////////////////////////////
        // GroupAccept///////////////////////////////////
        // GroupDismiss//////////////////////////////////
        // GroupKick/////////////////////////////////////
        // MJCPPackag////////////////////////////////////
        // UserUp////////////////////////////////////////
        // GroupCreate///////////////////////////////////
        // GroupDown/////////////////////////////////////
        else if(pack instanceof GroupDown){
            // System.out.println("成功接收退群报文！");
            // dispose();
            stopChat();
        }
        // GroupListUpdate///////////////////////////////
        // GroupMessage//////////////////////////////////
        else if(pack instanceof GroupMessage){
            GroupMessage message=(GroupMessage)pack;
            System.out.println(message.getSrc()+" :\n"+message.getContent());
            messageArea.append(message.getSrc()+" :\n"+message.getContent());endl();
        }
        // ServerDeny////////////////////////////////////
        else if(pack instanceof ServerDeny){
            MJCPPackage error = ((ServerDeny)pack).getError();
            if(error instanceof GroupInvite){
                String name=(( GroupInvite)error).getDst();
                messageArea.append("邀请 "+name+" 失败，可能是用户不存在或已经在群组中。");endl();
            }
        }
        // GroupDecline//////////////////////////////////
        // GroupExit/////////////////////////////////////
        // GroupInfoUpdate///////////////////////////////
        else if(pack instanceof GroupInfoUpdate) {
            ArrayList<String> newmembers = ((GroupInfoUpdate)pack).getGroupInfo().getUserList();
            System.out.println("群组 "+groupName+" 用户列表已经更新：");
            System.out.println(newmembers);
        }
        // GroupMemberDown//in GroupListUpdate///////////
        else if(pack instanceof GroupMemberDown) {
            String name = ((GroupMemberDown)pack).getClientName();
            if(removeMember(name)){
                messageArea.append(name+" 已退出群聊。");endl();
            }
        }
        // GroupMemberUp//in GroupListUpdate/////////////
        else if(pack instanceof GroupMemberUp){
            String name = ((GroupMemberUp)pack).getClientName();
            if(addMember(name)){
                messageArea.append(name+" 已加入群聊。");endl();
            }
        }
        // GroupUp///////////////////////////////////////
        // ServerHello///////////////////////////////////
    }

    public boolean reborn(){
        if(this.status){
            // messageArea.append("以上为历史消息");endl();
            this.status=true;
            return false;
        } else{
            // messageArea.append("以上为历史消息");endl();
            this.status=true;
            return true;
        }
    }

    private void endl(){
        messageArea.append("\n---------------------------\n");
    }
}
