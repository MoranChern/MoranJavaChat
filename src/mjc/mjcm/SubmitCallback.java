package mjc.mjcm;

import java.util.concurrent.Callable;

public abstract class SubmitCallback implements Callable<String>{
    protected MJCModule view;
    public void setSubmit(MJCModule submit){this.view=submit;}
    @Override
    public abstract String call() throws Exception;
}