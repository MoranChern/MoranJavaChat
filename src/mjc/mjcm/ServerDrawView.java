package mjc.mjcm;

import javax.swing.*;

import mjc.Server;
import mjc.mjcp.DrawPoint;
import mjc.mjcp.DrawRect;
import mjc.mjcp.DrawChangePen;
import mjc.mjcp.DrawClear;
import mjc.mjcp.DrawPenUp;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ServerDrawView extends JFrame {
    private Server caller;
    private JPanel drawingPanel;
    private Timer timer;
    private int radius=2;
    private Color color=Color.black;
    private int prevX, prevY;
    private int penStatus=0;

    public ServerDrawView(Server server) {
        this.caller = server;
        setTitle("教学白板（服务器端）");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setPreferredSize(new Dimension(400, 700));
        setLayout(new BorderLayout());

        // 创建绘图区域
        drawingPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                // 在这里实现绘图逻辑
            }
        };
        drawingPanel.setPreferredSize(new Dimension(400, 400));

        // 添加鼠标监听器以实现绘图功能
        drawingPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                // 鼠标按下时启动定时器
                prevX = e.getX();
                prevY = e.getY();
                if(penStatus==0){
                    timer.start();
                    caller.broadcast(new DrawPoint(prevX, prevY));
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                // 鼠标释放时停止定时器
                if(penStatus==0){
                    timer.stop();
                    caller.broadcast(new DrawPenUp());
                }else if(penStatus==1){
                    int w=e.getX()-prevX,h=e.getY()-prevY,xx,yy;
                    if(w<=0){
                        w=-w;
                        xx=e.getX();
                    }else{
                        xx=prevX;
                    }
                    if(h<=0){
                        h=-h;
                        yy=e.getY();
                    }else{
                        yy=prevY;
                    }
                    Graphics2D g = (Graphics2D) drawingPanel.getGraphics();
                    g.drawRect(xx, yy, w, h);
                    caller.broadcast(new DrawRect(xx,yy,w,h));
                }
            }
        });

        add(drawingPanel, BorderLayout.CENTER);

        JPanel bottomPanel = new JPanel(new BorderLayout());
        bottomPanel.setLayout(new FlowLayout());
        // 创建清空按钮
        JButton clearButton = new JButton("清空");
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // 清空绘图区域
                drawingPanel.repaint();
                caller.broadcast(new DrawClear());
            }
        });
        bottomPanel.add(clearButton);
        // 创建涂改带按钮
        JButton penButton = new JButton("板擦");
        penButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                color=new Color(238,238,238);
                radius=10;
                penStatus=0;
                caller.broadcast(new DrawChangePen(color, radius));
            }
        });
        bottomPanel.add(penButton);
        // 创建红笔按钮
        JButton redPenButton = new JButton("红笔");
        redPenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                color=Color.red;
                radius=2;
                penStatus=0;
                caller.broadcast(new DrawChangePen(color, radius));
            }
        });
        bottomPanel.add(redPenButton);
        // 创建黑笔按钮
        JButton BlackPenButton = new JButton("黑笔");
        BlackPenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                color=Color.black;
                radius=2;
                penStatus=0;
                caller.broadcast(new DrawChangePen(color, radius));
            }
        });
        bottomPanel.add(BlackPenButton);
        // 创建矩形按钮
        JButton eraserButton = new JButton("画矩形");
        eraserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                penStatus=1;
            }
        });
        bottomPanel.add(eraserButton);
        
        getContentPane().add(bottomPanel, BorderLayout.SOUTH);


        // 创建定时器，每隔5毫秒触发一次绘图事件
        timer = new Timer(5, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(penStatus==0){
                    // 在鼠标位置绘制一个圆形（示例）
                    int x = MouseInfo.getPointerInfo().getLocation().x - drawingPanel.getLocationOnScreen().x;
                    int y = MouseInfo.getPointerInfo().getLocation().y - drawingPanel.getLocationOnScreen().y;
                    if(x!=prevX||y!=prevY){
                        caller.broadcast(new DrawPoint(x, y));
                        Graphics2D g = (Graphics2D) drawingPanel.getGraphics();
                        g.setColor(color);
                        g.fillOval(x - radius, y - radius, radius*2, radius*2);

                        // 绘制当前点和上一个点的连线
                        g.setColor(color);
                        g.setStroke(new BasicStroke(radius*2));
                        g.drawLine(prevX, prevY, x, y);

                        prevX = x;
                        prevY = y;
                    }
                }else if(penStatus ==1){

                }
            }
        });

        pack();
        setResizable(false);
        setVisible(false);
    }
}