package mjc.mjcm;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import mjc.mjcp.LessonMessage;
import mjc.mjcp.MJCPPackage;

public class ClientLessonView extends MJCModule{
    
    private ObjectOutputStream chatOOS;
    private String clientName;

    private JTextArea messageArea;
    private JTextField inputField;

    public void stop(){
        if(this.isVisible()){
            // setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);  
            this.status=false;
            messageArea.append("对话已下线，请关闭此窗口\n---------------------------\n");
        }
        // else{
        //     this.dispose();
        // }
    }

    public ClientLessonView(String self,ObjectOutputStream out) {
        this.clientName = self;
        this.chatOOS = out;

        setTitle("课堂交流窗口（"+clientName+"）");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new BorderLayout());

        // 创建消息输出区域
        messageArea = new JTextArea();
        messageArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(messageArea);
        add(scrollPane, BorderLayout.CENTER);

        // 创建输入框区域
        inputField = new JTextField();
        inputField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(status&&!inputField.getText().isEmpty()){
                try {
                    chatOOS.writeObject(new LessonMessage(clientName, inputField.getText()));
                    // System.out.println("to " + target+" :\n"+inputField.getText());
                    // messageArea.append(clientName+" :\n"+inputField.getText()+"\n---------------------------\n");
                    inputField.setText("");
                } catch (IOException e1) {
                    System.err.println("发送信息到服务器时出现异常");
                    e1.printStackTrace();
                }}
            }
        });
        add(inputField, BorderLayout.SOUTH);

        pack();
        this.setSize(400, 400);
        setVisible(false);
        this.status=true;
    }

    public void push(MJCPPackage pack){    
        if(!this.status)  {;}  
        // ClientHello///////////////////////////////////
        // GroupDeny/////////////////////////////////////
        // GroupInvite///////////////////////////////////
        // GroupMemberUpdate/////////////////////////////
        // Message///////////////////////////////////////
        else if(pack instanceof LessonMessage){
            LessonMessage lessonMessage=(LessonMessage)pack;
            messageArea.append(lessonMessage.getSrc()+" :\n"+lessonMessage.getContent());endl();
        }
        // UserDown//////////////////////////////////////
        // GroupAccept///////////////////////////////////
        // GroupDismiss//////////////////////////////////
        // GroupKick/////////////////////////////////////
        // GroupMemberUp//in GroupListUpdate/////////////
        // MJCPPackag////////////////////////////////////
        // UserUp////////////////////////////////////////
        // GroupCreate///////////////////////////////////
        // GroupDown/////////////////////////////////////
        // GroupListUpdate///////////////////////////////
        // GroupMessage//////////////////////////////////
        // ServerDeny////////////////////////////////////
        // GroupDecline//////////////////////////////////
        // GroupExit/////////////////////////////////////
        // GroupMemberDown//in GroupListUpdate///////////
        // GroupUp///////////////////////////////////////
        // ServerHello///////////////////////////////////
    }
    private void endl(){
        messageArea.append("\n---------------------------\n");
    }
}
