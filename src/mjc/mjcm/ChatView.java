package mjc.mjcm;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import mjc.mjcp.*;

public class ChatView extends MJCModule{
    
    private ObjectOutputStream chatOOS;
    private String clientName;
    private String target;

    private JTextArea messageArea;
    private JTextField inputField;

    public String getClientName(){return this.target;}
    public void stop(){
        if(this.isVisible()){
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);  
            this.status=false;
            messageArea.append("对话已下线，请关闭此窗口\n---------------------------\n");
        }
        else{
            this.dispose();
        }
    }

    public ChatView(String self,String target, ObjectOutputStream out) {
        this.target = target;
        this.clientName = self;
        this.chatOOS = out;

        setTitle(target+" — 聊天窗口（"+clientName+"）");
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setLayout(new BorderLayout());

        // 创建消息输出区域
        messageArea = new JTextArea();
        messageArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(messageArea);
        add(scrollPane, BorderLayout.CENTER);

        // 创建输入框区域
        inputField = new JTextField();
        inputField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(status&&!inputField.getText().isEmpty()){
                try {
                    chatOOS.writeObject(new Message(clientName, target, inputField.getText()));
                    System.out.println("to " + target+" :\n"+inputField.getText());
                    messageArea.append(clientName+" :\n"+inputField.getText()+"\n---------------------------\n");
                    inputField.setText("");
                } catch (IOException e1) {
                    System.err.println("发送信息到 "+target+" 时出现异常");
                    e1.printStackTrace();
                }}
            }
        });
        add(inputField, BorderLayout.SOUTH);

        pack();
        this.setSize(400, 400);
        setVisible(false);
        this.status=true;
    }

    

    public void push(MJCPPackage pack){    
        if(!this.status)  {;}  
        // ClientHello///////////////////////////////////
        // GroupDeny/////////////////////////////////////
        // GroupInvite///////////////////////////////////
        // GroupMemberUpdate/////////////////////////////
        // Message///////////////////////////////////////
        else if(pack instanceof Message){
            Message message=(Message)pack;
            messageArea.append(message.getSrc()+" :\n"+message.getContent()+"\n---------------------------\n");
        }
        // UserDown//////////////////////////////////////
        else if(pack instanceof UserDown){
            stop();
        }
        // GroupAccept///////////////////////////////////
        // GroupDismiss//////////////////////////////////
        // GroupKick/////////////////////////////////////
        // GroupMemberUp//in GroupListUpdate/////////////
        // MJCPPackag////////////////////////////////////
        // UserUp////////////////////////////////////////
        // GroupCreate///////////////////////////////////
        // GroupDown/////////////////////////////////////
        // GroupListUpdate///////////////////////////////
        // GroupMessage//////////////////////////////////
        // ServerDeny////////////////////////////////////
        // GroupDecline//////////////////////////////////
        // GroupExit/////////////////////////////////////
        // GroupMemberDown//in GroupListUpdate///////////
        // GroupUp///////////////////////////////////////
        // ServerHello///////////////////////////////////
    }
}
