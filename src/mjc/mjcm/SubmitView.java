package mjc.mjcm;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class SubmitView extends MJCModule {
    private boolean disposablity=false;
    private SubmitCallback caller;
    private String value = null;
    private JLabel jl1 = null;
    private JTextField jtf1 = new JTextField();
    private JButton jb = new JButton("Submit");
    
    public String getValue(){return this.value;}

    public SubmitView(SubmitCallback callable,String title, String note,boolean allowDispose){
        this.disposablity = allowDispose;
        this.caller = callable;
        caller.setSubmit(this);

        this.setTitle(title);
        this.setSize(400, 150);
        if(this.disposablity){
            this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        }else{            
            this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);  
        }      
        this.setVisible(true);
        this.setLayout(null);
        
        if(note!=null){
            jl1=new JLabel(note);
            jl1.setBounds(0, 0, 400, 20);
            this.add(jl1);
        }
        jtf1.setBounds(0, 40, 400, 20);
        jtf1.addActionListener(new SubmitListener());
        this.add(jtf1);
        jb.setBounds(100, 80, 200, 20);
        jb.addActionListener(new SubmitListener());
        this.add(jb);
    }
    private class SubmitListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            if(status){
                jl1.setText("重复调用，请注意！");
            }else{
                value=jtf1.getText();
                status=true;
                setVisible(false);
                try {
                    caller.call();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    System.err.println("在确认界面回调时出现异常！");
                }
            }
        }
    }    

}