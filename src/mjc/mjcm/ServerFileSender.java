package mjc.mjcm;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
// import java.io.File;
// import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import mjc.mjcp.FileReceive;

public class ServerFileSender extends Thread{
    private String fileToSend=null;
    private String ipAddress=null;
    private int port=-1;
    private String clientName;

    public ServerFileSender(String file,FileReceive receiver)
    // throws FileNotFoundException
    {
        this.fileToSend=file;
        // File test=new File(file);
        // if(!test.exists()){throw new FileNotFoundException("文件不存在！");}
        this.clientName=receiver.getClientName();
        this.ipAddress=receiver.getIpAddress();
        this.port=receiver.getPort();
    }

    @Override
    public void run() {
        try (Socket socket = new Socket(ipAddress, port)) {
            byte[] buffer = new byte[4096];
            FileInputStream fis = new FileInputStream(fileToSend);
            BufferedInputStream bis = new BufferedInputStream(fis);
            OutputStream os = socket.getOutputStream();

            int bytesRead;
            while ((bytesRead = bis.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            os.flush();
            bis.close(); 
            System.out.println("已经发送文件到 " + this.clientName + " ！");
        }catch (IOException e) {
            System.err.println("发送文件到 " + this.clientName + " 时错误！");
        }
    }
}