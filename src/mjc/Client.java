package mjc;

import java.io.*;
import java.net.*;
import java.util.*;

import mjc.mjcm.*;
import mjc.mjcp.*;

public class Client {
/////////////////////////////////////////
public static void main(String[] args) {
    if (args.length>1){
        new Client(args[1]).runClient();
    }else{
        new Client().runClient();
    }
}
/////////////////////////////////////////

private String clientName=null;
private List<String> clientNames;
private List<GroupInfo> groups;

private boolean online=false;
private Socket socket;
private ObjectOutputStream out;
private Sender sender;
private ObjectInputStream in;


public Client(String name){
    this.clientName=name;
}
public Client(){}


private class Sender extends Thread{
    @Override
    public void run(){
        Scanner scanner=new Scanner(System.in);
        String dstName=null,messageText=null;
        boolean nameExist=false;
        while(online){
        while(!nameExist){
            dstName=scanner.next();
            messageText=scanner.nextLine();
            for(String name:clientNames){
                if(name.equals(dstName)){
                    nameExist=true;
                    break;
                }
            }
            if(!nameExist){
                System.out.println("用户不存在！");
            }
        }
        if(online) try{
            out.writeObject(new Message(clientName,dstName,messageText));
        }catch(IOException ex){
            System.out.println("发送器IO异常！");
            ex.printStackTrace();
        }
        else{
            System.out.println("服务器已离线！");
        }
        dstName=null;
        messageText=null;
        nameExist=false;
        }
        scanner.close();
    }
}

class ClientMainThread extends Thread{
    @Override
    public void run(){
        try  {            
            socket = new Socket(Config.SERVER_IP, Config.SERVER_PORT);
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("Connected to server");
            if(clientName == null){
                new SubmitView(new SetName(), "输入用户名", null,false);
            }
            else{
                out.writeObject(new ClientHello(clientName));
                Object obj = in.readObject();
                if(obj instanceof MJCPPackage){
                    if(obj instanceof ServerHello){
                        ServerHello serverHello=(ServerHello)obj;
                        clientNames=serverHello.getUserList();
                        groups=serverHello.getGroupList();
                        System.out.println("收到ServerHello");
                        System.out.println(clientNames);
                    }
                    else if(obj instanceof ServerDeny){
                        new SubmitView(new SetName(), "输入用户名", "不允许使用此用户名！",false);
                    }
                }
            }                
            System.out.println("正在创建客户端");
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    class SetName extends SubmitCallback{
        @Override
        public String call() throws Exception {
            clientName=((SubmitView)this.view).getValue();
            // System.out.println("Submit获取了一个值: " +clientName);
            this.view.dispose();
            try{
            out.writeObject(new ClientHello(clientName));
            Object obj = in.readObject();
            if(obj instanceof MJCPPackage){
                if(obj instanceof ServerHello){
                    ServerHello serverHello=(ServerHello)obj;
                    clientNames=serverHello.getUserList();
                    groups=serverHello.getGroupList();
                    System.out.println("收到ServerHello");
                    System.out.println(clientNames);

                    sender= new Sender();
                    sender.start();
                    System.out.println("User list:");
                    System.out.println(clientNames);
                    new ClientUI(clientName, clientNames, groups, in, out,socket);
                }
                else if(obj instanceof ServerDeny){
                    new SubmitView(this, "输入用户名", "不允许使用此用户名！",false);
                }
            }
            }catch (IOException | ClassNotFoundException e) {
                System.err.println("客户端命名过程遇到错误！");
            }
            return null;
        }
    }
}

public void runClient() {
    new ClientMainThread().start();
}
    
}