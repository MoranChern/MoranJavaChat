package mjc;

import java.io.*;
import java.net.*;
import java.util.*;
import mjc.mjcm.ServerDrawView;
import mjc.mjcm.ServerLessonView;
import mjc.mjcp.*;

public class Server {
/////////////////////////////////////////
public static void main(String[] args) {
    new Server().runServer();
}
/////////////////////////////////////////

ServerSocket serverSocket = null;
ServerLessonView lessonView = new ServerLessonView(this);
ServerDrawView drawView = new ServerDrawView(this);
private List<ClientHandler> clients = new ArrayList<>();
private ArrayList<String> clientNames = new ArrayList<>();
private ArrayList<String> clientBlackList = new ArrayList<>();
{clientBlackList.add("Server");clientBlackList.add("server");}
private List<ServerGroup> groups = new ArrayList<>();
private ArrayList<String> groupNames = new ArrayList<>();
private ArrayList<String> groupBlackList = new ArrayList<>();
boolean running = false;



protected void addClient(ClientHandler client){
    clients.add(client);
    clientNames.add(client.getClientName());
    try{
        for(ClientHandler c : clients){
            if(!c.getClientName().equals(client.getClientName()))
            {c.out.writeObject(new UserUp(client.getClientName()));}
            c.out.writeObject(new ServerHello(this.clientNames, clientGetGroup(c.getClientName())));
        }
    } catch(IOException ex){}
}

protected boolean checkUserName(String name){
    return !(clientNames.contains(name)||clientBlackList.contains(name));
}

protected void removeClient(ClientHandler client){
    for(ServerGroup group : groups){
        group.removeMember(client);
    }
    clientNames.remove(client.getClientName());
    clientBlackList.add(client.getClientName());
    clients.remove(client);
    try{
        client.in.close();
        client.out.close();
        client.socket.close();
    } catch(IOException ex){}
    for(ClientHandler c : clients){
        try{
            c.out.writeObject(new UserDown(client.getClientName()));
            c.out.writeObject(new ServerHello(clientNames, clientGetGroup(c.getClientName())));
        } catch(IOException ex){}
    }
}

protected void addGroup(ServerGroup group){
    groups.add(group);
    groupNames.add(group.getGroupName());
}

protected boolean checkGroupName(String name){
    return!(groupNames.contains(name)||groupBlackList.contains(name));
}

protected void removeGroup(ServerGroup group){
    groupNames.remove(group.getGroupName());
    groupBlackList.add(group.getGroupName());
    groups.remove(group);
}

public ArrayList<GroupInfo> clientGetGroup(String clientName){
    ArrayList<GroupInfo> ret=new ArrayList<>();
    for(ServerGroup group : groups){
        if(group.getMemberNames().contains(clientName)){
            ret.add(group.getGroupInfo());
        }
    }
    return ret;
}


private class InputTable extends Thread{
    @Override
    public void run() {
        Scanner scanner=new Scanner(System.in);
        while(running){
            System.out.print("MoranJavaChat> ");
            String arg1=scanner.nextLine();
            /*exit*///////////////////////////////
            if(arg1.equals("exit")){
                stopServer();
            }
            /*kick*///////////////////////////////
            else if(arg1.equals("kick")){
                System.out.print("Iuput User > ");
                String arg2=scanner.nextLine();
                boolean clientMatched=false;
                for (ClientHandler client : clients){                    
                    if(client.getClientName().equals(arg2)){
                        clientMatched=true;
                        // removeClient(client);
                        try {
                            client.out.writeObject(new ServerMessage("您已被强制下线！"));
                            client.socket.close();
                        } catch (IOException e) {}
                        // System.out.println(arg2+" 已强制下线");
                        break;
                    }
                }
                if(!clientMatched){
                    System.out.println("用户 "+arg2+" 不存在");
                }
            }
            /*dismiss*////////////////////////////
            else if(arg1.equals("dismiss")){
                System.out.print("Iuput Group > ");
                String arg2=scanner.nextLine();
                boolean groupMatched=false;
                for(ServerGroup group :groups){
                    if(group.getGroupName().equals(arg2)){
                        groupMatched=true;
                        removeGroup(group);
                        System.out.println("群聊 "+arg2+" 已强制解散");
                        try {
                            for(ClientHandler c : group.getMemberHandles()){
                                c.out.writeObject(new GroupDown(arg2));                                
                            }
                            group.getHostHandler().out.writeObject(new GroupDown(arg2));
                        } catch (IOException e) {}
                        break;
                    }
                }
                if(!groupMatched){
                    System.out.println("群聊 "+arg2+" 不存在");
                }
            }            
            /*client*///////////////////////////////
            else if(arg1.equals("client")){
                System.out.println(clientNames);
            }
            /*group*//////////////////////////////
            else if(arg1.equals("group")){
                System.out.println(groupNames);
            }
            /*group*//////////////////////////////
            else if(arg1.equals("broadcast")){
                System.out.println("broadcast> ");
                String arg2=scanner.nextLine();
                broadcast(new ServerMessage(arg2));
            }            
            /*draw*///////////////////////////////
            else if(arg1.equals("draw")){
                drawView.setVisible(true);
            }            
            /*lesson*/////////////////////////////
            else if(arg1.equals("lesson")){
                lessonView.setVisible(true);
            }            
            /*help*///////////////////////////////
            else if(arg1.equals("help")){
                System.out.println("exit kick dismiss client group broadcast draw lesson");
            }
            /*other*//////////////////////////////
            else{
                System.err.println("invalid command!");
            }
        }
        scanner.close();
    }
}

public void runServer() {
    try  {
        serverSocket = new ServerSocket(Config.SERVER_PORT);
        running = true;
        System.out.println("Server started. Listening on port " + Config.SERVER_PORT);
        new InputTable().start();
        while (running) {
            Socket clientSocket = serverSocket.accept();
            System.out.println("New client connected: " + clientSocket.getInetAddress().getHostAddress());
            ClientHandler clientHandler = new ClientHandler(clientSocket);
            clientHandler.start();
        }
    } catch (IOException ex) {
        ex.printStackTrace();
        System.out.println("ServerSocket部分捕获异常！可能有另一个Server实例正在运行！");
        System.exit(-1);
    }
}

public void stopServer(){
    running=false;
    System.out.println("Server is going to stop.");
    try {
        serverSocket.close();
    } catch (IOException e) {}
}

private class ServerGroup {    
    // TODO:检查此处memberNames是否一致是ArrayList
    private List<ClientHandler> waitList = new ArrayList<>();
    private List<ClientHandler> members = new ArrayList<>();
    private List<String> blackList = new ArrayList<>();
    private ArrayList<String> memberNames = new ArrayList<>();
    private String groupName=null;
    private String hostName=null;
    private ClientHandler hostHandler = null;
    public ServerGroup(String groupName,ClientHandler host){
        this.groupName=groupName;
        this.hostName=host.getClientName();
        this.hostHandler=host;
        // addMember(host);
    }
    public GroupInfo getGroupInfo(){
        return new GroupInfo(groupName,hostName, memberNames);
    }
    public String getHostName(){
        return this.hostName;
    }
    public ClientHandler getHostHandler(){
        return this.hostHandler;
    }
    public String getGroupName(){
        return this.groupName;
    }
    public List<String> getMemberNames(){return this.memberNames;}
    public List<ClientHandler> getMemberHandles(){
        return this.members;
    }
    // public void rename(String name){this.groupName=name;}
    // public void setGroupName(String name){this.groupName=name;}
    public void addMember(ClientHandler client){
        this.members.add(client);
        this.memberNames.add(client.getClientName());
        try {
            for(ClientHandler c : members){
                if(c==client){
                    c.out.writeObject(new GroupUp(getGroupInfo()));
                }
                else{
                    c.out.writeObject(new GroupMemberUp(client.clientName,this.groupName));
                    c.out.writeObject(new GroupInfoUpdate(this.getGroupInfo()));
                }                
            } 
            this.getHostHandler().out.writeObject(new GroupMemberUp(client.clientName,this.groupName));
            this.getHostHandler().out.writeObject(new GroupInfoUpdate(this.getGroupInfo()));
        } catch (IOException e) {}
    }
    public boolean removeMember(ClientHandler client){
        if(client.getClientName().equals(this.hostName)){return false;}
        boolean ret= this.members.contains(client) && this.memberNames.contains(client.getClientName());
        if(ret) try {for(ClientHandler c:members){ 
            blackList.add(client.getClientName());         
            if(c==client){
                c.out.writeObject(new GroupDown(this.groupName));
            }
            else{                    
                c.out.writeObject(new GroupMemberDown(client.clientName,this.groupName));
                c.out.writeObject(new GroupInfoUpdate(this.getGroupInfo()));
            }
            this.hostHandler.out.writeObject(new GroupMemberDown(client.clientName,this.groupName));
            this.hostHandler.out.writeObject(new GroupInfoUpdate(this.getGroupInfo()));
        }}catch (IOException e) {}
        this.members.remove(client);
        this.memberNames.remove(client.getClientName());
        return ret;
    }
    public boolean addWait(ClientHandler client){
        String name=client.getClientName();
        if(this.blackList.contains(name)||hostName.equals(name)||this.memberNames.contains(name)){
            return false;
        }else{
            this.waitList.add(client);
            return true;
        }
    }
    public boolean decline(ClientHandler client){
        return this.waitList.remove(client);
    }
    public boolean accept(ClientHandler client){
        boolean ret=this.waitList.remove(client);
        if(ret){
            addMember(client);
        }
        return ret;
    }
}

private class ClientHandler extends Thread {
    protected Socket socket;
    protected ObjectInputStream in;
    protected ObjectOutputStream out;
    protected String clientName=null;

    public String getClientName(){return this.clientName;} 
    
    public ClientHandler(Socket socket) {
        this.socket = socket;
        try {
            out = new ObjectOutputStream(socket.getOutputStream());
            in = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    @Override
    public void run() {
        try {
        while (running) {
        Object obj = in.readObject();
        System.out.println("收到来自 "+ clientName +" 的报文");
        if (obj instanceof MJCPPackage) {
            /* ClientHello *//////////////////////////////////
            if(obj instanceof ClientHello){
                ClientHello clientHello=(ClientHello)obj;
                //回复ServerHello
                if(checkUserName(clientHello.getClientName())){
                    this.clientName=clientHello.getClientName();
                    addClient(this);
                    // System.out.println(clients);
                }
                //回复ChangeName
                else{
                    this.out.writeObject(new ServerDeny(clientHello));
                    System.out.println("发送了一个ServerDeny到新加入用户");
                }
            }
            /* GroupAccept *//////////////////////////////////
            else if(obj instanceof GroupAccept){
                GroupAccept groupAccept=(GroupAccept)obj;
                boolean groupExist=false;
                for (ServerGroup group : groups){
                    if (group.getGroupName().equals(groupAccept.getGroupName())){
                        groupExist=true;
                        group.accept(this);
                        System.out.println(this.clientName+" 已加入群聊 " +group.getGroupName());                   
                        break;
                    }
                }
                if(!groupExist){
                    this.out.writeObject(new ServerDeny(groupAccept));
                }
            }
            /* GroupCreate *//////////////////////////////////
            else if(obj instanceof GroupCreate){
                GroupCreate groupCreate=(GroupCreate)obj;
                if(checkGroupName(groupCreate.getGroupName())){
                    ServerGroup newGroup = new ServerGroup(groupCreate.getGroupName(),this);
                    addGroup(newGroup);
                    this.out.writeObject(new GroupUp( newGroup.getGroupInfo()));
                    System.out.println(this.clientName+" 创建了群聊 "+groupCreate.getGroupName());
                }
                else{
                    this.out.writeObject(new ServerDeny(groupCreate));
                    System.out.println(this.clientName+" 创建群聊失败");
                }
            }
            /* GroupDecline */////////////////////////////////
            else if(obj instanceof GroupDecline) {
                GroupDecline groupDecline=(GroupDecline)obj;
                boolean groupExist=false;
                for(ServerGroup group : groups){
                    if(group.getGroupName().equals(groupDecline.getGroupName())){
                        if(group.decline(this)){System.out.println(this.clientName+" 拒绝加入群聊 " +group.getGroupName());}
                        groupExist=true;
                        break;
                    }
                }
                if(!groupExist){
                    this.out.writeObject(new ServerDeny(groupDecline));
                    this.out.writeObject(new ServerHello(clientNames, clientGetGroup(this.clientName)));
                }
            }
            /* GroupDeny *////////////////////////////////////
            // Not possile
            /* GroupDismiss */////////////////////////////////
            else if(obj instanceof GroupDismiss) {
                GroupDismiss groupDismiss=(GroupDismiss)obj;
                boolean groupExist=false;
                for(ServerGroup group : groups){
                    if(group.getGroupName().equals(groupDismiss.getGroupName())){
                        if(this.clientName.equals(group.getHostName())){
                            removeGroup(group);
                            for(ClientHandler client : group.getMemberHandles()){
                                client.out.writeObject(new GroupDown(group.getGroupName()));
                            }
                            this.out.writeObject(new GroupDown(group.getGroupName()));
                            System.out.println(this.clientName+" 解散了群聊 " +group.getGroupName());
                            break;
                        }
                        break;
                    }
                }
                if(!groupExist){
                    System.err.println("否认了来自 "+this.clientName+" 的报文");
                    this.out.writeObject(new ServerDeny(groupDismiss));
                    this.out.writeObject(new ServerHello(clientNames, clientGetGroup(this.clientName)));
                }
            }
            /* GroupDown *////////////////////////////////////
            // Not possile
            /* GroupExit *////////////////////////////////////
            else if(obj instanceof GroupExit) {
                GroupExit groupExit=(GroupExit)obj;
                for(ServerGroup group : groups){
                    if(group.getGroupName().equals(groupExit.getGroupName())){
                        group.removeMember(this);
                        System.out.println(this.clientName+" 退出群聊 " +group.getGroupName());
                    break;
                    }
                }
            }
            /* GroupInvite */////////////////////////////////
            else if(obj instanceof  GroupInvite ){
                GroupInvite groupInvite=(GroupInvite)obj;
                for(ServerGroup group : groups){
                if(group.getGroupName().equals(groupInvite.getGroupName())){
                    if(/*验证群主身份*/group.getHostName().equals(this.clientName)){
                        boolean allowInvite=false;
                        for(ClientHandler client : clients){
                            if(client.clientName.equals(groupInvite.getDst())){
                                if(group.addWait(client)){
                                    client.out.writeObject(groupInvite);
                                    System.out.println("转发了 "+this.clientName+" 发送给 "+ groupInvite.getDst()+" 加入 "+groupInvite.getGroupName()+" 群组的邀请");
                                    allowInvite=true;
                                }
                                break;
                            }
                        }

                        if(!allowInvite){
                            System.err.println("否认了来自 "+this.clientName+" 的报文");
                            this.out.writeObject(new ServerDeny(groupInvite));
                            this.out.writeObject(new GroupInfoUpdate(group.getGroupInfo()));
                        }
                    }
                    break;
                }}
            }
            /* GroupKick *////////////////////////////////////
            else if(obj instanceof  GroupKick){
                GroupKick groupKick=(GroupKick)obj;
                for(ServerGroup group : groups){
                if(group.getGroupName().equals(groupKick.getGroupName())){
                    //验证群主身份
                    if(group.getHostName().equals(this.clientName)){
                        boolean nameExist=false;
                        for(ClientHandler client : clients){
                            nameExist=true;
                            if(client.clientName.equals(groupKick.getDst())){
                                group.removeMember(client);
                                System.out.println(groupKick.getDst()+" 被 "+this.clientName+" 移出了群聊 "+groupKick.getGroupName());
                                break;
                            }
                        }
                        if(!nameExist){
                            System.err.println("否认了来自 "+this.clientName+" 的报文");
                            this.out.writeObject(new ServerDeny(groupKick));
                            this.out.writeObject(new GroupInfoUpdate(group.getGroupInfo()));
                        }
                    }
                    break;
                }}
            }
            /* GroupLeave *////////////////////////////////////
            /* GroupListUpdate *//////////////////////////////
            // Not possile
            /* GroupMemberDown *//////////////////////////////
            // Not possile
            /* GroupMemberUp *////////////////////////////////
            // Not possile
            /* GroupMessage */////////////////////////////////
            else if(obj instanceof GroupMessage) {
                GroupMessage groupMessage=(GroupMessage)obj;
                for(ServerGroup group : groups){
                    if(group.getGroupName().equals(groupMessage.getGroupName())){
                        // 发送消息给群成员
                        for(ClientHandler client : group.getMemberHandles()){
                            client.out.writeObject(groupMessage);
                            System.out.println(groupMessage.getSrc()+" 发送了群消息到 "+groupMessage.getGroupName());
                        }
                        // 发送消息给群主
                        group.hostHandler.out.writeObject(groupMessage);
                        break;
                    }
                }
            }
            /* GroupUp *//////////////////////////////////////
            // Not possile
            /* Message *//////////////////////////////////////
            else if(obj instanceof Message) {
                Message message=(Message)obj;
                String dst=message.getDst();
                boolean nameExist=false;
                for (ClientHandler client : clients){
                    if (client.clientName.equals(dst)){
                        nameExist=true;
                        client.out.writeObject(message);
                        System.out.println("转发了 "+this.clientName+" 发送给 "+message.getDst()+ " 的消息");
                        break;
                    }
                }
                //用户不存在
                if(!nameExist){
                    this.out.writeObject(new ServerDeny(message));
                    this.out.writeObject(new ServerHello(clientNames, clientGetGroup(this.clientName)));
                }
            }
            /* MJCPPackage *//////////////////////////////////
            // Not possile
            /* ServerDeny *///////////////////////////////////
            // Not possile
            /* ServerHello *//////////////////////////////////
            // Not possile
            /* UserDown */////////////////////////////////////
            // Not possile
            /* UserUp *///////////////////////////////////////
            // Not possile
            else if(obj instanceof FileReceive){
                System.out.println("收到文件请求");
                lessonView.push((FileReceive)obj);
            }
            else if(obj instanceof LessonMessage){
                LessonMessage msg = (LessonMessage) obj;
                broadcast(msg);
                lessonView.push(msg);
            }
        }
        }
        } catch(EOFException | SocketException e){
            System.out.println(this.clientName+"已下线");
            removeClient(this);
        }
        catch (IOException | ClassNotFoundException e) {
            removeClient(this);
            System.out.println("服务端捕获到异常！ "+this.clientName+" 已下线");
            e.printStackTrace();
        }
    }
}

public void broadcast(MJCPPackage pack){    
    for(ClientHandler c:clients){
        try {
            c.out.writeObject(pack);
        } catch (IOException e) {
            System.err.println("服务器广播消息发送失败！");
        }
    }
}

}