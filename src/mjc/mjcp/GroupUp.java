package mjc.mjcp;

public class GroupUp extends MJCPPackage{
    private GroupInfo list;    
    public GroupUp(GroupInfo groupList){
        this.list=groupList;
    }
    public GroupInfo getGroupInfo(){
        return this.list;
    }
    public String getGroupName(){
        return this.list.getGroupName();
    }
}