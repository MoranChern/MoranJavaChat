package mjc.mjcp;

public class GroupMessage extends MJCPPackage{
    protected String src,dst,content;
    public GroupMessage(String from,String to,String text){
        this.src=from;
        this.dst=to;
        this.content=text;
    }
    public String getSrc(){
        return this.src;
    }
    public String getDst(){
        return this.dst;
    }
    public String getGroupName(){
        return this.dst;
    }
    public String getContent(){
        return this.content;
    }
}