package mjc.mjcp;

public class Message extends MJCPPackage{
    protected String src,dst,content;
    public Message(String from,String to,String text){
        this.src=from;
        this.dst=to;
        this.content=text;
    }
    public String getSrc(){
        return this.src;
    }
    public String getDst(){
        return this.dst;
    }
    public String getContent(){
        return this.content;
    }
}