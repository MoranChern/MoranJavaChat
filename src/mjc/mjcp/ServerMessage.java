package mjc.mjcp;

public class ServerMessage extends MJCPPackage{
    private String text;
    public ServerMessage(String content){
        this.text = content;
    }
    public String getMsg(){
        return this.text;
    }
}
