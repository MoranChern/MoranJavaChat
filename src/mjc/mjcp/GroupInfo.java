package mjc.mjcp;

import java.io.Serializable;
import java.util.ArrayList;

public class GroupInfo implements Serializable {
    protected String groupName = null;
    protected String hostName = null;
    protected ArrayList<String> clientNames = new ArrayList<>();

    public GroupInfo(String groupName, String hostName, ArrayList<String> clientNames) {
        this.clientNames = clientNames;
        this.groupName = groupName;
        this.hostName = hostName;
    }

    public String getGroupName() {
        return this.groupName;
    }
    public String getHostName() {
        return this.hostName;
    }

    // public void rename(String name){this.name=name;}
    // public void setGroupName(String name){this.name=name;}
    public ArrayList<String> getUserList() {
        return this.clientNames;
    }

    // public void add(String name) {
    //     this.clientNames.add(name);
    // }

    // public void remove(String name) {
    //     this.clientNames.remove(name);
    // }
}