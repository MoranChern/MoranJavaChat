package mjc.mjcp;

public class GroupInvite extends MJCPPackage {
    protected String src,dst,content;
    public GroupInvite(String host,String to,String groupName){
        this.src=host;
        this.dst=to;
        this.content=groupName;
    }
    public String getSrc(){
        return this.src;
    }
    public String getDst(){
        return this.dst;
    }
    public String getGroupName() {return this.content;}
    public String getHostName() {return this.src;}
}