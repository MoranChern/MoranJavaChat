package mjc.mjcp;

public class GroupInfoUpdate extends MJCPPackage{
    private GroupInfo list;
    public GroupInfoUpdate(GroupInfo groupList){
        this.list=groupList;
    }
    public GroupInfo getGroupInfo(){
        return this.list;
    }
}