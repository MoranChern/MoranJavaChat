package mjc.mjcp;

public class DrawRect  extends MJCPPackage{
    int x,y,h,w;
    public DrawRect(int X,int Y,int width,int height){
        this.x = X;this.y = Y;this.h = height;this.w = width;
    }
    public int getX(){return this.x;}
    public int getY(){return this.y;}
    public int getWidth(){return this.w;}
    public int getHeight(){return this.h;}
}
