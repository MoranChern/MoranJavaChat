package mjc.mjcp;

public class ClientHello extends MJCPPackage {
    protected String name;
    public ClientHello(String text){
        this.name=text;
    }
    public String getClientName(){
        return this.name;
    }
}
