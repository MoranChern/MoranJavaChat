package mjc.mjcp;

public class ServerDeny extends MJCPPackage { 
    MJCPPackage mjcpPackage;
    public ServerDeny(MJCPPackage mjcpPackage) {
        this.mjcpPackage = mjcpPackage;
    }
    public MJCPPackage getError(){return this.mjcpPackage;}
}
