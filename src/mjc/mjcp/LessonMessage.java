package mjc.mjcp;

public class LessonMessage extends MJCPPackage{
    protected String src,content;
    public LessonMessage(String from,String text){
        this.content=text;
        this.src=from;
    }
    public String getSrc(){
        return this.src;
    }
    public String getContent(){
        return this.content;
    }
}