package mjc.mjcp;

public class GroupMemberDown extends MJCPPackage{
    private String clientName;
    private String groupName;

    public GroupMemberDown(String client,String group) {
        this.clientName=client;
        this.groupName=group;
    }
    public String getClientName(){return this.clientName;}
    public String getGroupName(){return this.groupName;}
}
