package mjc.mjcp;

public class GroupMemberUp extends MJCPPackage{
    private String clientName;
    private String groupName;

    public GroupMemberUp(String client,String group) {
        this.clientName=client;
        this.groupName=group;
    }
    public String getClientName(){return this.clientName;}
    public String getGroupName(){return this.groupName;}
}
