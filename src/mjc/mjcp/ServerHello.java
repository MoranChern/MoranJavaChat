package mjc.mjcp;
import java.util.ArrayList;

public class ServerHello extends MJCPPackage {
    protected ArrayList<String> userList;
    protected ArrayList<GroupInfo> groupList;
    public ServerHello(ArrayList<String> userList,ArrayList<GroupInfo> groupList){
        this.userList=userList;
        this.groupList=groupList;
    }
    public ArrayList<String> getUserList(){
        return this.userList;
    }
    public ArrayList<GroupInfo> getGroupList(){
        return this.groupList;
    }
}