package mjc.mjcp;

public class FileReceive extends MJCPPackage {
    private String ipAddress;
    private int port;
    private String clientName;
    public FileReceive(String name,String ip ,int port) {
        this.clientName=name;this.ipAddress=ip;this.port=port;
    }
    public String getIpAddress(){return this.ipAddress;}
    public int getPort(){return this.port;}
    public String getClientName(){return this.clientName;}
}