package mjc.mjcp;

import java.awt.Color;

public class DrawChangePen extends MJCPPackage {
    private Color c;
    private int r;
    public DrawChangePen(Color color, int radius){
        this.c = color;this.r = radius;
    }
    public Color getColor(){return this.c;}
    public int getRadius(){return this.r;}
}