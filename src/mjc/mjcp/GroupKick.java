package mjc.mjcp;

public class GroupKick extends MJCPPackage {
    protected String src,dst,content;
    public GroupKick(String host,String to,String groupName){
        this.src=host;
        this.dst=to;
        this.content=groupName;
    }
    public String getSrc(){
        return this.src;
    }
    public String getDst(){
        return this.dst;
    }
    public String getContent(){
        return this.content;
    }
    public String getGroupName() {return this.content;}
    public String getHostName() {return this.content;}
}